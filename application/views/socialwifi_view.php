

<?php $this->load->view('header_view', array('title' => 'Social WiFi')); ?>

<div class="website-100">
    <div class="product-socialwifi-slider">
        <div class="product-slider-box-text">
            <h1 style="color:#ffffff; padding:0px;margin:0px">Social WiFi</h1>
            <div class="sliders-subtitle">Enhance the user experience. Boost your brand presence and gather a wealth of user demographics.</div>
            <div class="product-slider-button-1"><a style="color:#fff" href="http://splash.brandfi.co.ke">Get Started</a>
            </div>
            <div class="product-slider-button-2 hidden"><a href="//purple.ai/contact/our-sales-team/">Contact sales</a></div>
        </div>
    </div>
    <div class="product-white-100">
        <div class="product-200-right">
            <h2 style="font-size:30px;padding:0px;margin:0px">Social login</h2>
            <div class="section-subtitle">A frustration-free way for guests to access your WiFi network</div>
            <p></p>
            <div class="section-main-image-left"><img src="//purple.ai/wp-content/themes/oshin/img/upload/socialwifi-main.png" alt="Social WiFi login"></div>
            <div class="section-main-text">Guests expect a quick and simple process when they log in to your WiFI network. Nobody wants to complete lengthy sign up forms or go through multiple pages to get online.<p></p>
                <p>Puples’ social WiFi feature allows customer to access your network by their preferred social media profile at the press of a button. </p>
                <p>The login page encourages users to connect with your brand socially, immediately boosting your social media presence. All done in a secure and legally compliant way.</p>
                <p>What’s more, once connected; your business leverages rich, accurate data so you can understand who your guests really are and drive meaningful marketing campaigns.</p></div>
        </div>
    </div>
    <div class="product-lightgrey-100">
        <div class="product-200-right">
            <div class="section-title">Marketing via social WiFi</div>
            <div class="section-subtitle">Build bespoke marketing campaigns that are highly relevant to your users</div>
            <p></p>
            <div class="section-main-image-left"><img src="//purple.ai/wp-content/themes/oshin/img/upload/socialwifi-02.png"></div>
            <p></p>
            <div class="section-main-text">Once a user has logged in through a social media profile such as<br>
                Facebook, you’ll unleash all the demographic information on their public profile such as gender, age and current city. You’ll also have visibility on the first and the last time they visited the venue.<p></p>
                <p>With our innovative, built in marketing tools you’ll be able to use this<br>
                    valuable information to send beautifully designed eShots and SMS that are highly targeted and relevant. </p>
                <div class="blue-button" onclick="location.href = '<?= base_url('wifi/wifi-marketing'); ?>';"><a href="<?= base_url('wifi/wifi-marketing'); ?>">View More On WiFi Marketing</a></div>
            </div>
        </div></div>
    <div class="c2a-100">
        <div class="product-200-right">
            <div class="c2a-title">We transform  WiFi networks across the world</div>
            <div class="c2a-subtitle">Find out why customers turn to Social WiFi with Brandfi</div>
            <p> </p>
            <div class="c2a-button"><a style="color:#fff" href="http://splash.brandfi.co.ke" target="_blank">Take a 14 Day Trial</a></div>
            <p></p></div>
    </div>
    <div class="product-white-100">
        <div class="product-200-right">
            <div class="section-title">Social WiFi case study</div>
            <div class="section-subtitle">Delivering an immediate social media boost through our social WiFi experience</div>
            <p></p>
            <div class="section-main-image-left"><img src="//purple.ai/wp-content/themes/oshin/img/upload/socialwifi-03.png"></div>
            <div class="section-main-text">A great example of social WiFi providing an instant boost to a customer’s business is explored in our case study for Llechwedd Slate Caverns, a popular tourist attraction in Snowdonia, Wales.<p></p>
                <p>Since installing social WiFi at the caverns, the marketing team has reported the following results on social media:</p>
                <ul class="blue-tick">
                    <li>A 500% increase in Facebook page likes</li>
                    <li>Average number of views for each Facebook post has increased by 200%, from 400 to over 1,200</li>
                    <li>Viral impressions, linked to posts, likes, sharing and comments, have now exceeded 175,000</li>
                    <li>A substantial increase in follower numbers on both Twitter and Instagram</li>
                </ul>
            </div>
            <p></p>
            <div class="blue-button-center hidden" onclick="location.href = '//purple.ai/casestudies/lsc/';"><a href="//purple.ai/casestudies/lsc/">View The Full Case Study</a></div>
        </div></div>
    <div class="product-lightgrey-100">
        <div class="product-200-right">
            <div class="section-title">Benefits of social WiFi</div>
            <div class="benefits-subtitle">Promote your business &amp; communicate with your customers on a whole new level</div>
            <p> </p>
            <div class="benefits-box">
                <div class="benefits-icon"> <img src="//purple.ai/wp-content/themes/oshin/img/upload/socialwifi-icon-01.png"><br>
                    Guests are able to access fast, secure internet through social WiFi logins
                </div>
                <div class="benefits-icon"> <img src="//purple.ai/wp-content/themes/oshin/img/upload/socialwifi-icon-02.png"><br>
                    Social login available through Facebook, Twitter, LinkedIn and Instagram
                </div>
                <div class="benefits-icon"> <img src="//purple.ai/wp-content/themes/oshin/img/upload/socialwifi-icon-03.png"><br>
                    Immediately boost your social profile and presence to a wider network
                </div>
                <div class="benefits-icon"> <img src="//purple.ai/wp-content/themes/oshin/img/upload/socialwifi-icon-04.png"><br>
                    Reach friends of friends through social media logins and activity
                </div>
                <div class="benefits-icon"> <img src="//purple.ai/wp-content/themes/oshin/img/upload/socialwifi-icon-05.png"><br>
                    Easily promote offers and events to<br>
                    existing and potential customers
                </div>
                <div class="benefits-icon"> <img src="//purple.ai/wp-content/themes/oshin/img/upload/socialwifi-icon-06.png"><br>
                    Drive key engagement metrics including reach, likes and shares
                </div>
            </div>
        </div>
    </div>
    
    
    
<?php $this->load->view('floating_view'); ?>
    

</div>

<?php $this->load->view('footer_view'); ?>
