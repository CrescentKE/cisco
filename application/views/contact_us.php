

<?php $this->load->view('header_view', array('title' => 'Brandfi | Get WiFi Analytics')); ?>


<div class="v2sliderco">
    <div class="banner-text">
        <h1 style="padding-top: 60px;
            font-weight: 400;
            color: #fff;
            ">Contact Us</h1>
        <div class="v2slider-text">Submit the form below and we will get back to you</div>
        <p>&nbsp;</p>
    </div>
</div>



<div class="v2showcase">
    <div class="v2showcase-title">Contact Form</div>
    <div class="v2showcase-sub">Fill in all the details</div>
    <div class="v2showcase-sub">Landmark plaza 13th floor &CenterDot; <a href="mailto:Info@brandfi.co.ke">info@brandfi.co.ke</a></div>
    
    <div class="container-fluid">
        <div class="col-md-6 col-md-offset-3">
            <form method="post" action="" id="contactForm" style="text-align: left">
                <div class="form-group">
                    <label>Your email</label>
                    <input type="email" class="form-control" name="cemail" id="cemail" placeholder="Email address" required=""/>
                </div>
                <div class="form-group">
                    <label>Subject</label>
                    <input type="text" class="form-control" name="csubject" id="csubject" placeholder="Subject" required=""/>
                </div>
                <div class="form-group">
                    <label>Message</label>
                    <textarea name="cmessage" id="cmessage" required="" placeholder="Your message" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-support-nav">Send Message</button>
                </div>
            </form>
        </div>
    </div>
</div>
<br/>
<br/>

<div class="v2users">
    <div class="v2users-title">We transform WiFi networks across the world</div>
    <div class="v2slider-button2">
    </div>
</div>

<?php $this->load->view('footer_view'); ?>
