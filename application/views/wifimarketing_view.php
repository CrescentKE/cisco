

<?php $this->load->view('header_view', array('title' => 'Brandfi | Get WiFi Analytics')); ?>



<?php $this->load->view('floating_view'); ?>


<div class="website-100">
    <div class="product-emailsms-slider">
        <div class="product-slider-box-text">
            <h1 style="color: #ffffff; padding: 0px; margin: 0px;">WiFi Marketing</h1>
            <div class="sliders-subtitle">Automated marketing through WiFi</div>
            <div class="product-slider-button-1"><a style="color: #fff;" href="http://splash.brandfi.co.ke">Get started with WiFi marketing</a></div>
            <div class="product-slider-button-2 hidden"><a href="//purple.ai/contact/our-sales-team/">Contact sales</a></div>
        </div>
    </div>
    <div class="product-white-100">
        <div class="product-200-right">
            <h2 style="font-size: 30px; padding: 0px; margin: 0px;">Email &amp; SMS</h2>
            <div class="section-subtitle">Send targeted messages from the WiFi data which you collect</div>
            <p></p>
            <div class="section-main-text">
                <p>With our WiFi marketing tools your can send emails, SMS and coupons in real-time to visitors in your venue. Create highly targeted messages and engage with your consumers based on behavior and demographics. The result? Incredibly bespoke marketing campaigns.</p>
                <p><img src="//purple.ai/wp-content/themes/oshin/img/upload/email-01.png" alt="WiFi marketing" style="float: left; padding-right: 20px; padding-bottom: 40px;"><b>Logic Flow</b><br>
                    Logic Flow is a powerful drag and drop builder providing a simple and clear visual of how your WiFi marketing automation is setup and what actions are set to trigger.</p>
            </div>
            <div class="section-main-text" style="width: 250px;">
                <div class="email-page-icon-01"><b>It’s their birthday!</b><br>
                    Send personalized coupons offering a discount on their birthday meal.</div>
                <p></p>
                <div class="email-page-icon-02"><b>It’s raining!</b><br>
                    Text customers directing them to the nearest store that is selling umbrellas.</div>
                <p></p>
                <div class="email-page-icon-03"><b>Near a store with a sale on?</b><br>
                    Send marketing SMS to let them know they’re near a store with a sale on.</div>
            </div>
            <p></p>
            <div class="blue-button-center hidden" onclick="location.href = '//purple.ai/wifi/logicflow/';"><a href="//purple.ai/wifi/logicflow/">View More On Logic Flows</a></div>
        </div>
    </div>
    <div class="product-charcoal-100">
        <div class="product-200-right">
            <div class="product-charcoal-100-title">SMS Messages</div>
            <div class="section-subtitle-white">Enterprise SMS marketing</div>
            <p></p>
            <div class="product-charcoal-100-paragraph"><img src="//purple.ai/wp-content/themes/oshin/img/upload/email-02.png" style="float: left; padding-right: 20px; padding-top: 20px;"></div>
            <div class="product-charcoal-100-paragraph" style="width: 250px;">
                <p>Our WiFi marketing software makes it easy to send a highly targeted SMS in real-time to customers who are either in your venue or have previously visited.</p>
                <p>Send individual messages or large-scale text marketing campaigns straight from the Brandfi Portal and instantly engage with customers.</p>
                <p><img src="//purple.ai/wp-content/themes/oshin/img/upload/email-03.png" style="float: left; padding-right: 20px;"></p>
            </div>
            <p>&nbsp;</p>
            <div class="blue-button-center"><a style="color: #fff;" href="http://splash.brandfi.co.ke">Try Email &amp; SMS Marketing For 14 Days</a></div>
        </div>
    </div>
    <div class="product-white-100">
        <div class="product-200-right">
            <div class="section-title">Email</div>
            <div class="section-subtitle">Simple to use drag and drop email editor</div>
            <p></p>
            <div class="section-main-image-left"><img src="//purple.ai/wp-content/themes/oshin/img/upload/email-04.png"></div>
            <div class="section-main-text" style="width: 250px;">
                <p>The WiFi marketing tools in the Brandfi Portal are so easy to use that email design and build becomes effortless. Our eShot editor allows you to select templates, colors and fonts enabling your team to build beautiful email without any coding. What’s more, our templates are fully mobile responsive so no matter which device your customers are using, your email will always look great.Segment your customer using the data you’ve collated through your WiFi. Define the demographics such as age, gender and even the last time they visited the venue.</p>
            </div>
        </div>
    </div>
    <div class="product-lightgrey-100">
        <div class="product-200-right">
            <div class="section-title">Benefits of WiFi Marketing </div>
            <div class="benefits-subtitle">Here’s a few benefits of using your guest WiFi to access data and send communications</div>
            <p></p>
            <div class="benefits-box">
                <div class="benefits-icon"><img src="//purple.ai/wp-content/themes/oshin/img/upload/benefits-sms-v2-01.png"><br>
                    Easily create branded splash pages with our drag and drop editor or HTML tool</div>
                <div class="benefits-icon"><img src="//purple.ai/wp-content/themes/oshin/img/upload/benefits-sms-v2-02.png"><br>
                    Segment and redirect users as they login to your WiFi by their demographics</div>
                <div class="benefits-icon"><img src="//purple.ai/wp-content/themes/oshin/img/upload/benefits-sms-v2-03.png"><br>
                    Send targeted emails and SMS based on demographic data and behavior</div>
                <div class="benefits-icon"><img src="//purple.ai/wp-content/themes/oshin/img/upload/benefits-sms-v2-04.png"><br>
                    Reach friends of friends through social<br>
                    media logins and activity</div>
                <div class="benefits-icon"><img src="//purple.ai/wp-content/themes/oshin/img/upload/benefits-sms-v2-05.png"><br>
                    Easily promote offers and events to<br>
                    existing and potential customers</div>
                <div class="benefits-icon"><img src="//purple.ai/wp-content/themes/oshin/img/upload/benefits-sms-v2-06.png"><br>
                    Drive key engagement metrics including reach, likes and shares</div>
            </div>
        </div>
    </div>
    <div class="c2a-100">
        <div class="product-200-right">
            <div class="c2a-title">Unlock data and send messages with WiFi marketing</div>
            <div class="c2a-subtitle">Find out why customers turn to Brandfi</div>
            <p></p>
            <div class="c2a-button"><a style="color: #fff;" href="http://splash.brandfi.co.ke">Take a 14 Day Trial</a></div>
        </div>
    </div>
</div>

<?php $this->load->view('footer_view'); ?>
