

<?php $this->load->view('header_view', array('title' => 'WiFi Analytics')); ?>


<?php $this->load->view('floating_view'); ?>

<div class="website-100">
    <div class="product-reports-slider">
        <div class="product-slider-box-text">
            <h1 style="color: #ffffff; padding: 0px; margin: 0px;">WiFi Analytics</h1>
            <div class="sliders-subtitle">Measure your visitors behavior in real-time</div>
            <div class="product-slider-button-1"><a style="color: #fff;" href="http://splash.brandfi.co.ke">Get Started</a></div>
            <div class="product-slider-button-2 hidden"><a href="//purple.ai/contact/our-sales-team/">Contact sales</a></div>
        </div>
    </div>
    <div class="product-white-100">
        <div class="product-200-right">
            <div class="section-title">Brandfi Portal</div>
            <div class="section-subtitle">Web analytics for the real, physical world</div>
            <p></p>
            <div class="section-main-image-left"><img src="//purple.ai/wp-content/themes/oshin/img/upload/reports-01.png"></div>
            <div class="section-main-text">
                With our cloud software enabled over your existing network you can access a wealth of rich WiFi analytics. Similar to web analytics, our platform displays real­-time footfall, passers by, conversion and bounce rates, dwell times, return visits and frequency; enabling businesses to truly understand their venues. Using WiFi, Bluetooth and GPS we can enable additional location services to pinpoint customer movement in venues down to a few metres. Businesses can then send hyperlocal marketing messages to people as they move around the venue in real-time.<p></p>
                <p>What’s more, with the right setup, we can capture footfall numbers, and visitor analytics even if visitors don’t log in to the Guest WiFi.</p>
                <p>We use a restful API, so the data displayed in the Brandfi Portal can be connected to other information systems such as CRM and ERP.</p>
            </div>
            <p></p>
            <div class="blue-button-center"><a style="color: #fff;" href="http://splash.brandfi.co.ke">Take a 14 Day Trial</a></div>
        </div>
    </div>
    <div class="product-lightgrey-100">
        <div class="product-200-right">
            <div class="section-title">Benefits</div>
            <div class="benefits-subtitle">A few benefits of WiFi analytics</div>
            <p></p>
            <div class="benefits-box">
                <div class="benefits-icon"><img src="//purple.ai/wp-content/themes/oshin/img/upload/benefits-wifianalytics-v2-01.png"><br>
                    Website Analytics for the real, physical world</div>
                <div class="benefits-icon"><img src="//purple.ai/wp-content/themes/oshin/img/upload/benefits-wifianalytics-v2-02.png"><br>
                    Understand how demographics move in your space</div>
                <div class="benefits-icon"><img src="//purple.ai/wp-content/themes/oshin/img/upload/benefits-wifianalytics-v2-03.png"><br>
                    Measure the dwell time of visitors in your venue</div>
                <div class="benefits-icon"><img src="//purple.ai/wp-content/themes/oshin/img/upload/benefits-wifianalytics-v2-04.png"><br>
                    Access real-time data at a venue, company or group level</div>
                <div class="benefits-icon"><img src="//purple.ai/wp-content/themes/oshin/img/upload/benefits-wifianalytics-v2-05.png"><br>
                    Create highly targeted marketing campaigns</div>
                <div class="benefits-icon"><img src="//purple.ai/wp-content/themes/oshin/img/upload/benefits-wifianalytics-v2-06.png"><br>
                    Create operational efficiencies to save money and boost revenue</div>
            </div>
        </div>
    </div>
    <div class="product-white-100">
        <div class="product-200-right">
            <div class="section-title">Reports</div>
            <div class="section-subtitle">Meaningful graphs that tell a story</div>
            <p></p>
            <div class="section-main-image"><img src="//purple.ai/wp-content/themes/oshin/img/upload/reports-02.png"></div>
            <p></p>
            <div class="white-bg-paragraph">
                <p>Visual graphs can be created and accessed at any time within the portal. WiFi analytics can provide user demographics such as age, gender and location, length of internet use, total amount of users and data usage are just some of the statistics you can collect. This deeper level of analytics will enable you to deploy your marketing messages to the right people, at the right time.For example, if a cafe using Brandfi to provide their guest WiFi knows that a customer has logged in more than four times this month, they could reward them for their loyalty with a personalized offer such as a free cup of their favorite coffee. The cafe can even apply this when a regular customer has their birthday, or they bring a friend who also logs into the WiFi.</p>
            </div>
            <div class="white-bg-paragraph">
                <p>The same principle can also be used on a much bigger scale in larger venues. For example, stadiums can use the WiFi analytics to drive ticket sales for season passes and push fans towards the concession stands for high margin products like popcorn and drinks. Entire cities can send highly targeted, relevant information to tourists and transport network users, offering a positive experience and streamlining movement around the busiest areas.The use cases for WiFi analytics in venues of all sizes are limitless.</p>
            </div>
            <p></p>
            <div class="blue-button-center" onclick="location.href = '#';"><a href="#/">Try WiFi analytics today</a></div>
        </div>
    </div>
    <div class="c2a-100">
        <div class="product-200-right">
            <div class="c2a-title">We transform  WiFi networks across the world</div>
            <div class="c2a-subtitle">Find out why customers turn to Brandfi</div>
            <p></p>
            <div class="c2a-button"><a style="color: #fff;" href="http://splash.brandfi.co.ke">Take a 14 Day Trial</a></div>
        </div>
    </div>
</div>

<?php $this->load->view('footer_view'); ?>
