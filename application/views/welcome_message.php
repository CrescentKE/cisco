

<?php $this->load->view('header_view', array('title' => 'Brandfi | Get WiFi Analytics')); ?>


<div class="v2slider">
    <div class="banner-text">
        <h1 style="padding-top: 60px;
            font-weight: 600;
            color: #fff;
            ">BRAND-fi Connected Digital Experience</h1>
        <div class="v2slider-text" style="font-weight: 500;">Subscription-based, Engaging cloud-delivered ,Wi-Fi connect,<br/> Analytics and Digital Signage solutions</div>
        <p>&nbsp;</p>
        <div class="v2slider-button hidden"><a href="//purple.ai/videos/" id="home-watchvideo">Watch video</a></div>
        <div class="v2slider-button2 hidden-xs hidden-sm"><a href="<?= base_url('contactus'); ?>" id="home-signup">Contact us Today</a></div>
    </div>
</div>


<div class="v2boxmain">
    <div class="v2box1">
        <p class="h33">Connect</p>
        <p class="v2box-text">Responsive Splash Pages</p>
    </div>
    <div class="v2box2">
        <p class="h33">Analytics</p>
        <p class="v2box-text">Analytics and Reporting</p>
    </div>
    <div class="v2box3">
        <p class="h33">Engage</p>
        <p class="v2box-text">Engagement Tools</p>
    </div>
    <div class="v2box4">
        <p class="h33">Signage</p>
        <p class="v2box-text">Intelligent Signage</p>
    </div>
</div>


<div class="v2returns hidden">
    <div class="v2returns-title">Measurable return from customer insights</div>
    <div class="v2returns-sub">Ranging from 
        <a id="home-casestudies">hospitality</a>, <a id="home-casestudies">retail</a>, <a id="home-casestudies">leisure</a> and <a id="home-casestudies">healthcare</a> centers</div>
    <div class="v2returns-box">
        <p><a href="//purple.ai/casestudies/pizza-express-uae/" id="home-casestudies">
                <img class="img-responsive" src="//purple.ai/wp-content/themes/oshin/img/uploadv2/insight-01.jpg" alt="Guest wifi in Pizza Express"></a></p>
        <div class="v2returns-text"><span style="font-size: 30px; color: #4A8456;">181%</span><br>
            Increase in page impressions in the first 4 weeks<br>
        </div>
    </div>
    <div class="v2returns-box">
        <p><a href="//purple.ai/casestudies/the-eldan-hotel/" id="home-casestudies">
                <img class="img-responsive" src="//purple.ai/wp-content/themes/oshin/img/uploadv2/insight-03.jpg" alt="Tripadvisor in Eldan Hotel"></a></p>
        <div class="v2returns-text"><span style="font-size: 30px; color: #4A8456;">218%</span><br>
            Increase in the number of TripAdvisor reviews collected<br>
        </div>
    </div>
    <div class="v2returns-box">
        <p><a href="//purple.ai/casestudies/wokandgo/" id="home-casestudies">
                <img class="img-responsive" src="//purple.ai/wp-content/themes/oshin/img/uploadv2/insight-02.jpg" alt="Wok&amp;Go case study"></a></p>
        <div class="v2returns-text"><span style="font-size: 30px; color: #4A8456;">186%</span><br>
            Increase in customers connecting to their guest WiFi<br>
        </div>
    </div>
</div>


<div class="v2showcase">
    <div class="v2showcase-title">Industry showcase</div>
    <div class="v2showcase-sub">Learn how Brandfi delivers results in your industry</div>
    <div class="v2showcase-box">
        <div class="v2scbox1">
            <div class="v2sc-title">Restaurants and bars</div>
            <p>Bars, restaurants and hotels can increase revenue, update guests and add value to venues<br>
        </div>
        <div class="v2scbox3">
            <div class="v2sc-title">Retail and malls</div>
            <p>Increase revenue, update shoppers and encourage social engagement with Brandfi<br>
        </div>
        <div class="v2scbox2">
            <div class="v2sc-title">Hotels</div>
            <p>Bars, restaurants and hotels can increase revenue, update guests and add value to venues<br>
        </div>
        <div class="v2scbox4">
            <div class="v2sc-title">Stadiums and leisure</div>
            <p>Improve the fan experience with live updates, in-game advertising and partnerships<br>
        </div>
    </div>
</div>

<div class="v2users">
    <div class="v2users-title">API Technology Partners</div>
    <div class="v2users-titlesub">API integrations into the following partners:</div>
    <div class="v2slider-button2 text">
        Twitter, Facebook, PayPal, Safaricom API, Cisco Meraki, Mikrotik,Ubiquiti and Ruckus
    </div>
</div>

<?php $this->load->view('footer_view'); ?>
