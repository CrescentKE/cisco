<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= $title; ?></title>

        <!-- Bootstrap -->
        <link href="<?= base_url('pop/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
        <link href="<?= base_url('pop/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?= base_url('pop/css/wifi.min.css'); ?>" rel="stylesheet">

        <link rel="shortcut icon" href="<?= base_url('pop/imgs/favicon.ico'); ?>" type="image/x-icon">
        <link rel="icon" href="<?= base_url('pop/imgs/favicon.ico'); ?>" type="image/x-icon">


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>


        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?= base_url(); ?>">BRAND-fi</a>
                </div>


                <div class="collapse navbar-collapse" id="main-nav-collapse">
                    <button type="button" class="btn btn-default navbar-btn navbar-right btn-support-nav">Setup Guides</button>
                    <button class="btn btn-default navbar-btn navbar-right btn-signup-nav hidden" onclick="location.href = 'http://splash.brandfi.co.ke'">Free Sign Up</button>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                Digital Solutions 
                            </a>
                            <ul class="dropdown-menu">
                                <li><a onclick="location.href = '<?= base_url('wifi/wifianalytics'); ?>'">WiFi Analytics</a></li>
                                <li><a onclick="location.href = '<?= base_url('wifi/wifi-marketing'); ?>'">WiFi Marketing</a></li>
                            </ul>
                        </li>
                        <li><a href="<?= base_url('contactus') ?>">Contact Us</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>