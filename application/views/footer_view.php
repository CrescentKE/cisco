
<footer id="bottom-widgets">
    <div id="bottom-widgets-wrap" class="be-wrap be-row clearfix">
        <div class="one-fourth col-md-4 column-block clearfix">
            <div class="widget_nav_menu widget">
                <h6 class="hidden">About Us</h6>
                <div class="menu-footer-menu-1-about-purple-container">
                    <ul id="menu-footer-menu-1-about-purple" class="menu">
                        <li id="menu-item-11601" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11601">
                            <a href="#">About Us</a>
                        </li>
                    </ul></div></div>					</div>
        
        <div class="one-fourth col-md-4 column-block clearfix text-center">
            <div class="widget_text widget"><h6 class="hidden">Contact Us</h6>		
                <div class="textwidget">
<!--                    <i class="fa fa-phone-square"></i> +254 722 721563<br/>
                    <i class="fa fa-envelope-square"></i> info@brandfi.co.ke<br/>
                    <i class="fa fa-map-marker"></i> Landmark Plaza 13<sup>TH</sup> Floor
                    <br>
                    <br>-->
                    <div class="icon-shortcode align-none">
                        <a href="https://fb.me/brandfi" class="icon-shortcode icon-square  " data-animation="fadeIn" target="_blank">
                            <i class="font-icon fa fa-facebook tiny square" style="border-style: solid; border-width: 1px; border-color: #654e83; background-color: inherit; color: #fff;" data-animation="fadeIn" data-bg-color="inherit" data-hover-bg-color="#654e83" data-color="#fff" data-hover-color="#ffffff" data-border-color="#654e83" data-hover-border-color="#654e83"></i>
                        </a>
                    </div>
                    <div class="icon-shortcode align-none">
                        <a href="https://twitter.com/brandfi2" class="icon-shortcode icon-square  " data-animation="fadeIn" target="_blank">
                            <i class="font-icon fa fa-twitter tiny square" style="border-style: solid; border-width: 1px; border-color: #654e83; background-color: inherit; color: #fff;" data-animation="fadeIn" data-bg-color="inherit" data-hover-bg-color="##654e83" data-color="#fff" data-hover-color="#ffffff" data-border-color="#654e83" data-hover-border-color="#654e83"></i>
                        </a>
                    </div>
                </div>
            </div>					</div>
        <div class="one-fourth col-md-4 column-block clearfix">
            <div class="widget_icl_lang_sel_widget widget"><h6 class="hidden">Language selector</h6>
                <div class="textwidget pull-right">
<!--                    <i class="fa fa-phone-square"></i> +254 722 721563<br/>
                    <i class="fa fa-envelope-square"></i> info@brandfi.co.ke<br/>
                    <i class="fa fa-map-marker"></i> Landmark Plaza 13<sup>TH</sup> Floor
                    <br>
                    <br>-->
                    <div class="icon-shortcode align-none">
                        <a href="<?= base_url('contactus');?>" class="icon-shortcode icon-square  " data-animation="fadeIn" target="_blank">
                           Contact Us
                        </a>
                    </div>
                    <div class="icon-shortcode align-none">
                        <a href="#terms" class="icon-shortcode icon-square  " data-animation="fadeIn" target="_blank">
                            Terms
                        </a>
                    </div>
                </div>
            </div>					
        </div>
    </div>
</footer>


<footer id="footer" class="layout-wide">
    <span class="footer-border be-wrap "></span>
    <div id="footer-wrap" class=" style1 be-wrap clearfix">
        <div class="footer-left-area">				</div>
        <div class="footer-center-area">				</div>
        <div class="footer-right-area">					<div class="footer-content-inner-right">				                    Copyright Brandfi 2017. All Rights Reserved                					</div>					</div>
    </div>
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?= base_url('pop/js/jquery.min.js'); ?>"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?= base_url('pop/js/bootstrap.min.js'); ?>"></script>
</body>
</html>