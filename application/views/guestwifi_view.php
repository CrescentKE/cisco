

<?php $this->load->view('header_view', array('title' => 'Guest WiFi')); ?>


<?php $this->load->view('floating_view'); ?>

<div class="website-100">
    <div class="product-wifi-slider">
        <div class="product-slider-box-text">
            <h1 style="color: #ffffff; padding: 0px; margin: 0px;font-weight: 100;">Guest WiFi</h1>
            <div class="sliders-subtitle">Gain valuable insights around physical spaces and promote your business like never before.</div>
            <div class="product-slider-button-1"><a style="color: #fff;font-weight: 100;" href="http://splash.brandfi.co.ke">Get Started</a></div>
            <div class="product-slider-button-2 hidden"><a href="//purple.ai/contact/our-sales-team/">Contact sales</a></div>
        </div>
    </div>
    <div class="product-white-100">
        <div class="product-200-right">
            <h2 style="font-size: 30px; padding: 0px; margin: 0px;">Guest WiFi as it should be</h2>
            <div class="section-subtitle">Gain valuable insights around physical spaces and promote your business like never before</div>
            <p></p>
            <div class="section-main-image-left"><img src="//purple.ai/wp-content/themes/oshin/img/upload/wifi-mobile.png" alt="Guest WiFi"></div>
            <div class="section-main-text">Today, successful businesses make the most noise about their customer experience, and we think guest WiFi should play an integral part in this.<p></p>
                <p>Make it beautiful, branded and seamless for customers to login just once across all your venues and you’re on to a winner. Turn on social login too, and drive your social engagement through the roof.</p>
                <p>Our cloud software provides an overlay over existing hardware and can be up and running within an hour.</p>
                <div class="blue-button" onclick="location.href = '<?= base_url('social-wifi');?>';"><a href="<?= base_url('social-wifi');?>">Learn More About Social WiFi</a></div>
            </div>
        </div>
    </div>
    <div class="product-charcoal-100">
        <div class="product-200-right">
            <div class="product-charcoal-100-title">WiFi marketing</div>
            <div class="section-subtitle-white">Marketing tools in conjunction with your guest WiFi service</div>
            <p></p>
            <div class="product-charcoal-100-paragraph">The WiFi marketing tools available in the Brandfi portal provide a comprehensive method to monitor and actively promote your business. You can tailor your splash (login) pages with specific branding and advertising for your guest WiFi users and send targeted emails and SMS. The URL redirect page they land on after logging in is also<br>
                completely within your control.<br>
                <img src="//purple.ai/wp-content/themes/oshin/img/upload/wifi-logic.png" style="float: left; padding-right: 20px; padding-top: 20px;"><p></p>
                <p><b>Logic flow</b><br>
                    LogicFlow is Brandfi’s new, easy to use drag and drop marketing tool.</p>
                <p>Setup automated campaigns in minutes based on user behavior, location and even the weather.</p></div>
            <div class="product-charcoal-100-paragraph2">Features at a glance<p></p>
                <p><img src="//purple.ai/wp-content/themes/oshin/img/upload/marketing-icon-01.png" style="float: left; padding-right: 10px;padding-bottom: 50px"><b>Splash pages</b><br>
                    Create branded splash pages for your guest WiFi visitors with our drag and drop editor</p>
                <p><img src="//purple.ai/wp-content/themes/oshin/img/upload/marketing-icon-03.png" style="float: left; padding-right: 10px;padding-bottom: 50px"><b>Reviews</b><br>
                    Connect review platforms and send feedback request to visitors who connect to your guest WiFi</p>
                <p><img src="//purple.ai/wp-content/themes/oshin/img/upload/marketing-icon-02.png" style="float: left; padding-right: 10px;padding-bottom: 50px"><b>Email &amp; SMS</b><br>
                    Send emails &amp; SMS based on demographics and behavior</p>
                <div class="blue-button" onclick="location.href = '<?= base_url('wifi/wifi-marketing');?>';">
                    <a href="<?= base_url('wifi/wifi-marketing');?>">View WiFi Marketing</a></div>
            </div>
        </div>
    </div>
</div>

<div class="product-white-100">
    <div class="product-200-right">
        <div class="section-title">Reporting and Insights</div>
        <div class="section-subtitle">Analytics for physical spaces, or as we call it… Website Analytics for the real world</div>
        <div class="wibeacon-img"><img src="//purple.ai/wp-content/themes/oshin/img/upload/wifi-video.png"></div>
        <div class="white-bg-paragraph">With our ‘Brandfi Portal’, you can access a wealth of information collated from the Brandfi system. Our analytics platform provides real-time guest WiFi data and insight. Secure and available at any time, a vast array of tools will help you analyze and segment your audience</div>
        <div class="white-bg-paragraph">Real time data can be accessed at any time. User demographics such as age, gender and location, length of internet use, total amount of users and data usage are just some of the statistics you can collect.</div>
        <p>&nbsp;</p>
        <div class="blue-button-center" onclick="location.href = '<?= base_url('wifi/wifianalytics');?>';">
            <a href="<?= base_url('wifi/wifianalytics');?>">View More On WiFi Reporting</a>
        </div>
    </div>
</div>

<div class="stretch-purple-100">
    <div class="product-200-right">
        <div class="stretch-purple-100-width">
            <div class="stretch-purple-100-title">Content filtering</div>
            <div class="stretch-purple-100-subtitle">Guest WiFi comes with the risk that consumers can potentially access content that is not familyfriendly. Brandfi has partnered with Open DNS to provide a convenient content filtering system which blocks inappropriate material.</div>
           
        </div>
    </div>
</div>

<div class="product-lightgrey-100">
    <div class="product-200-right">
        <div class="section-title">Bandwidth management</div>
        <div class="section-subtitle">Bill guests for guest WiFi usage after a period of free time, or bill them for superior bandwidth</div>
        <p></p>
        <div class="section-main-text">Time – You can set a limit to the amount of free access time on the guest WiFI, 30 minutes for example. When the limit is reached and usage continues, a charge to the guest is incurred.Speed – A limit can be set on the upload or download speed on the guest WiFi. Users then have the option to upgrade for a faster WiFI experience at a charge.<p></p>
            <p>Usage – A limit to the usage can be set. For example, a guest who is simply checking their emails or browsing online will not use much bandwidth and therefore will not be charged, but a guest who is streaming videos (and using a higher proportion of the allocated bandwidth) would be charged.</p>
        </div>
        <div class="section-main-text" style="font-family: montserrat; color: #222; font-size: 20px; line-height: 32px;">
            <p>Our cloud software gives you complete control and flexibility to manage your bandwidth. You can quickly and easily edit the block of paid time you want to offer, the price you charge or manage the speed and usage.</p>
            <p><img src="//purple.ai/wp-content/themes/oshin/img/upload/bandwidth-01.png"></p>
        </div>
    </div>
</div>

<div class="c2a-100">
    <div class="product-200-right">
        <div class="c2a-title">We transform  guest WiFi networks across the world</div>
        <div class="c2a-subtitle">Find out why customers turn to Brandfi</div>
        <p></p>
        <div class="c2a-button"><a style="color: #fff;" href="http://splash.brandfi.co.ke" target="_blank">Take a 14 Day Trial</a></div>
    </div>
</div>

<?php $this->load->view('footer_view'); ?>
