<div class="floatmenu-container hidden-sm hidden-xs">
    <div class="floatmenu">
        <div class="menubutton-wifi" onclick="location.href = '<?= base_url('guest-wifi'); ?>';"><a href="<?= base_url('guest-wifi'); ?>">WiFi Solutions</a></div>
        <div class="menubutton" onclick="location.href = '<?= base_url('social-wifi'); ?>';"><a href="<?= base_url('social-wifi'); ?>">Social WiFi</a></div>
        <div class="menubutton hidden" onclick="location.href = '/wifi/logicflow/';"><a href="/wifi/logicflow/">Logic flow</a></div>
        <div class="menubutton" onclick="location.href = '<?= base_url('wifi/wifi-marketing'); ?>';"><a href="<?= base_url('wifi/wifi-marketing'); ?>">Marketing tools</a></div>
        <div class="menubutton hidden" onclick="location.href = '/wifi/micro-surveys/';"><a href="/wifi/micro-surveys/">Micro surveys</a></div>
        <div class="menubutton hidden" onclick="location.href = '/wifi/nps-surveys/';"><a href="/wifi/nps-surveys/">NPS surveys</a></div>
        <div class="menubutton hidden" onclick="location.href = '/wifi/reviews/';"><a href="/wifi/reviews/">Reviews</a></div>
        <div class="menubutton hidden" onclick="location.href = '/wifi/salesforce/';"><a href="/wifi/salesforce/">Salesforce connector</a></div>
        <div class="menubutton hidden" onclick="location.href = '/wifi/contentfiltering/';"><a href="/wifi/contentfiltering/">Content filtering</a></div>
        <div class="menubutton hidden" onclick="location.href = '/wifi/bandwidthmanagement/';"><a href="/wifi/bandwidthmanagement/">Bandwidth &amp; payments</a></div>
        <div class="menubutton hidden" onclick="location.href = '/wifi/multilingual/';"><a href="/wifi/multilingual/">Multilingual</a></div>
        <div class="menubutton" onclick="location.href = '<?= base_url('wifi/wifianalytics');?>';"><a href="<?= base_url('wifi/wifianalytics');?>">WiFi analytics</a></div>
        <div class="menubutton hidden" onclick="location.href = '/wifi/api/';"><a href="/wifi/api/">API access</a></div>
        <div class="menubutton-byod hidden" onclick="location.href = '/wifi/locationservices/';"><a href="/locationservices/">Location services</a></div>
        <div class="menubutton hidden" onclick="location.href = '/wifi/wibeacons/';"> <a href="/wifi/wibeacons/">Wibeacons</a></div>
        <div class="menubutton hidden" onclick="location.href = '/wifi/heatmapping/';"><a href="/heatmapping/">Heat mapping</a></div>
        <div class="menubutton hidden" onclick="location.href = '/wifi/moving-venues/';"><a href="/wifi/moving-venues/">Moving Venues</a></div>
    </div>
</div>